# helm-deps-visualizer

Utility to visualize Helm charts dependencies.

## Setup

```bash
pip install .
```

## Requirements

* Graphviz

## Usage

```bash
# Specify tgz file
$ helm-deps-visualizer visualize <tgz-helm-chart>
# Specify directory
$ helm-deps-visualizer visualize <helm-chart-folder>
```