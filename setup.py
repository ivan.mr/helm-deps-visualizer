import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="helm-deps-visualizer", # Replace with your own username
    version="0.0.1",
    author="Ivan Martinez",
    author_email="ivan.samartino@gmail.com",
    description="Helm chart dependencies visualizer",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ivan.mr/helm-deps-visualizer",
    package_dir={'': 'src'},
    packages=setuptools.find_packages(where='src'),
    install_requires=[
        'click==7.1.1',
        'pyyaml==5.3.1',
        'pydotplus==2.0.2',
        'Pillow>=7'
        ],
    entry_points={
        'console_scripts': [
            'helm-deps-visualizer=cli:cli'
        ],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
)
