from flask import Flask, request, render_template, flash, redirect, url_for, send_from_directory
from werkzeug.utils import secure_filename
from visualizer.parser import Parser
import os

UPLOAD_FOLDER = '/tmp/helm-vis/'
ALLOWED_EXTENSIONS = {'tgz'}


app = Flask(__name__)
app.secret_key = "something-secret"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

def visualize_chart():
    return 'Something sent via POST'

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    error = None
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            chart_file = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(chart_file)
            parser = Parser(chart_file)
            img_file = parser.run(store=app.config['UPLOAD_FOLDER'])
            return redirect(url_for('uploaded_file',
                                    filename=os.path.basename(img_file)))
        else:
            error = f'Please provide a valid format. Allowed formats: {ALLOWED_EXTENSIONS}'
    return render_template('home.html', error=error)