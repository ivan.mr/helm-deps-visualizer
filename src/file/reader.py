import tarfile
import utils.log as log
import tempfile
from pathlib import Path
from abc import ABC, abstractmethod
from os import getcwd
from shutil import copytree, Error


class Reader(ABC):
    def create_temp_dir(self):
        log.info('Creating temporary directory')
        t = tempfile.TemporaryDirectory(prefix='.helm-visualizer', dir="/tmp")
        log.debug(f'Created temporary directory: {t.name}')
        return t

    @abstractmethod
    def to_temp_dir(self):
        pass

class TarballReader(Reader):
    def __init__(self, path):
        if not tarfile.is_tarfile(path):
            log.error(f'{path} is not a tarfile')
            raise tarfile.ReadError()
        self.tf_path = path
        self.tf = None

    def to_temp_dir(self):
        t = self.create_temp_dir()
        self.tf = tarfile.open(self.tf_path)
        self.tf.extractall(path=t.name)
        log.debug(f'Extracted files to {t.name}')
        self.tf.close()
        return t

class DirReader(Reader):
    def __init__(self, path):
        if not (path.is_dir() and path.exists()):
            log.error('Could not process directory')
            raise FileNotFoundError()
        self.p = path

    def to_temp_dir(self):
        t = self.create_temp_dir()
        p_src = Path(self.p)
        p_dst = Path(t.name)

        if p_src.resolve() == p_dst.parents[0].resolve():
            log.error(f'Change location of target directory to avoid recursive copy')
            raise Exception('Unable to generate temporary directory')

        try:
            log.debug("Copying files to temporary directory")
            copytree(self.p, t.name, dirs_exist_ok=True)
        except Error as e:
            log.error(f'Directory could not be copied: {e}')
        except OSError as e:
            log.error(f'Directory could not be copied: {e}')
        return t

