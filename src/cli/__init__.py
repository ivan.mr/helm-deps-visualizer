import click

from pathlib import Path

from file.reader import TarballReader, DirReader
from utils.log import debug, info, warn, error
from visualizer.parser import Parser

@click.group()
def cli():
    pass

@click.command()
@click.argument('chart', type=click.Path(exists=True,
    file_okay=True,
    dir_okay=True))
def visualize(chart):
    debug('begin::visualize')
    parser = Parser(chart)
    parser.run(interactive=True)



cli.add_command(visualize)
