import click
import datetime

levels = {
        'debug': 'DEBUG',
        'info': 'INFO',
        'warn': 'WARNING',
        'error': 'ERROR',
    }

def log_decorator(func):
    def wrapper(m):
        s = (
            f"[{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}]"
            f"[{levels.get(func.__name__)}]: {m}"
            )
        func(s)
    return wrapper

@log_decorator
def debug(m):
    click.echo(click.style(m, fg='green'))

@log_decorator
def info(m):
    click.echo(click.style(m, fg='blue'))

@log_decorator
def warn(m):
    click.echo(click.style(m, fg='yellow'))

@log_decorator
def error(m):
    click.echo(click.style(m, fg='red'))
