from helm.chart import Chart
from pathlib import Path
import pydotplus
from utils.log import *

class GraphGen():
    def __init__(self, c:Chart):
        self.c = c
        self.graph = pydotplus.Dot(graph_type="graph",
                               rankdir="LR",
                               graph_name=c.name,
                               nodeset='0.15',
                               size='30.0,30.0',
                               dpi='150.0',
                               fontcolor="red")
        self.create_graph()

    def add_edge(self, root, child):
        d = {
               'fontsize': '12',
               'fontcolor': 'red',
               'height': '10.0'
            }
        r = pydotplus.Node(name=root, **d)
        c = pydotplus.Node(name=child, **d)
        edge = pydotplus.Edge(r, c)
        edge.set_weight('1.2')
        self.graph.add_edge(edge)

    def add_childs(self, c: Chart):
        if c.deps is not None:
            for dep in c.deps:
                self.add_edge(f"{c.name}-{c.version}",
                              f"{dep.name}-{dep.version}")
                self.add_childs(dep)

    def create_graph(self):
        if len(self.c.deps) == 0:
            warn(f"{self.c.name} does not have deps to visualize")
            self.add_edge(f"{self.c.name}-{self.c.version}", "No dependencies")
        else:
            self.add_childs(self.c)

    def write(self, p:str):
        pth = Path(p)
        if pth.parent.is_dir():
            self.graph.write(str(pth), format='jpg')
