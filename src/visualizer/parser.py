from visualizer.graph_gen import GraphGen
from functools import singledispatchmethod
from file.reader import TarballReader, DirReader
from helm.chart import Chart
from os import listdir, scandir
from os.path import isfile
from pathlib import  Path
from PIL import Image
import subprocess
import sys
import tarfile
from utils.log import debug, info, warn, error
import yaml

class Parser():
    def __init__(self, p):
        self.recursion_level = 0
        self.p = Path(p)
        if self.p.is_dir():
            info('chart is a directory')
            self.r = DirReader(self.p)
        else:
            info('chart is a tarball')
            self.r = TarballReader(self.p)
        # Top level Helm chart object
        self.top_level = Chart()
        self.env = {
                'chart': 'Chart.yaml',
                'deps': 'requirements.yaml',
                'subfolder': 'charts'
                }

    def is_valid_chart(self,p):
        debug(f"Processing path: {p}")
        cfp = ChartFileProvider(p)
        if isfile(cfp.chart):
            debug("Found Helm chart file")
            debug(f"{cfp.chart}")
            # Initialize the top_level Chart attribute to pass a reference that will be modified
            self.top_level.update(self.read_chart(cfp.chart))
            info("Top Level Chart")
            info(self.top_level)
            return p
        # Recursive processing if it is a directory
        elif p.is_dir():
            warn("Processing subdirectory file")
            for subp in scandir(p):
                result = self.is_valid_chart(subp)
                # Check the result and return path from recursion
                # Otherwise continue the execution
                if result:
                    return result
        return False


    def parse_requirements(self, c: Chart, p:Path):
        cfp = ChartFileProvider(p)
        if isfile(cfp.reqs):
            self.recursion_level += 1
            # Check directory and the fact that is not empty (should contain the dependencies)
            if cfp.charts.is_dir() and listdir(str(cfp.charts)):
                c.deps = self.read_reqs(cfp.reqs)
                for dep in c.deps:
                    subchart_path = cfp.charts / Path(dep.name)
                    # Untar the dependency if it  is compressed
                    if not subchart_path.is_dir():
                        warn(f"Subchart path is not a directory\n"
                             f"Checking for compressed files")
                        self.untar_dep(subchart_path, dep)
                    # TODO: There could be duplicated dependencies?
                    sub_cfp = ChartFileProvider(subchart_path)
                    dep.update(self.read_chart(sub_cfp.chart))
                    self.parse_requirements(dep, subchart_path)
            else:
                warn(
                    f"Chart: {self.top_level.name} has a requirements.yaml\n"
                    f"but {cfp.env['subfolder']} subfolder doesn't exist or is empty"
                )
                exit(1)
        else:
            debug(f"{c.name} does not have dependencies. Stopping recursion")


    def read_chart(self, p: Path):
        with p.open(mode='r') as f:
           yml = yaml.load(f, Loader=yaml.FullLoader)
           try:
               # Improve the parsing to include rest of parameters
               c = Chart(name=yml.get("name"), version=yml.get("version"),
                       api_version=yml.get("apiVersion"))
               return c
           except Exception as e:
               error(f"Error parsing Helm chart: {str(p)}")
               raise e

    def read_reqs(self, p:Path):
        r = []
        with p.open(mode='r') as f:
            try:
                yml = yaml.load(f, Loader=yaml.FullLoader)
                for dep in yml['dependencies']:
                    r.append(
                                Chart(dep['name'],
                                dep['version'],
                                repository=dep['repository'])
                            )
                    debug(f"Parsed dependency:\n{r[-1]}")
            except Exception as e:
                error(f"Could not parse dependencies from: {p.name}")
                error(f"{e}")
                exit(1)
        return r

    def run(self, store=None, interactive=False):
        tmp_dir = self.process(self.r)
        debug(f'Validating Chart')
        chart_path = self.is_valid_chart(Path(tmp_dir.name))
        info(f"Chart_path is:{chart_path}")
        if chart_path:
            self.parse_requirements(self.top_level, Path(chart_path))
            info(f"Recursion level is:{self.recursion_level}")
            debug(f"TODO: Show all the info about the processed helm chart")
            g = GraphGen(self.top_level)
            if store:
                debug(f"Storing Helm Chart")
                stored_img = Path(store) / f"{self.top_level.name}_{self.top_level.version}.jpg"
                g.write(str(stored_img))
                return str(stored_img)
            if interactive:
                debug(f"Running interactive mode")
                tmp_png = Path(tmp_dir.name) / f"{self.top_level.name}_{self.top_level.version}.jpg"
                g.write(str(tmp_png))
                imgViewer = {'linux': 'xdg-open',
                        'win32': 'expolorer',
                        'darwin': 'open'}[sys.platform]
                subprocess.run([imgViewer, tmp_png])
                input("Press enter to end")

    def untar_dep(self, p:Path, c:Chart):
        tar_file = p.parent / f"{c.name}-{c.version}.tgz"
        if not (tar_file.is_file() and tarfile.is_tarfile(tar_file)):
            error(f"File {str(tar_file)} not recognized as a compressed file")
            exit(1)
        tf = tarfile.open(tar_file)
        tf.extractall(path=p.parent)
        tf.close()

    # Making use of singledispatchmethod to enable polymorphism
    @singledispatchmethod
    def process(self, r: 'DirReader'):
        debug("init::parse Directory")
        return r.to_temp_dir()

    @process.register
    def _(self, r: 'TarballReader'):
        debug("init::parse Tarball")
        return r.to_temp_dir()


class ChartFileProvider():
    def __init__(self, p: Path):
        self.env = {
                'chart': 'Chart.yaml',
                'deps': 'requirements.yaml',
                'subfolder': 'charts'
                }
        self.chart = p / Path(self.env['chart'])
        self.reqs = p / Path(self.env['deps'])
        self.charts = p / Path(self.env['subfolder'])
