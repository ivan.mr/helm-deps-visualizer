from utils.log import *

class Chart():
  def __init__(self,
          name = None,
          version = None,
          api_version=None,
          description = None,
          app_version = None,
          repository = None,
          deps=None):
    self.name = name
    self.version = version
    # Chart could act as a dependency and therefore hold this value
    self.repository = repository
    self.api_version = api_version
    self.description = description
    self.app_version = app_version
    self.deps = [] if deps is None else deps

  def __str__(self):
    return (
      f"\nApi Version: {self.api_version}\n"
      f"Name: {self.name}\n"
      f"Version: {self.version}\n"
      f"Repository: {self.repository}\n"
      f"App Version: {self.app_version}\n"
      f"Deps: {self.deps}\n"
    )

  def update(self, c):
    own_attrs = [a for a in dir(self) if not a.startswith('__') and not callable(getattr(self, a))]
    other_attrs = [a for a in dir(c) if not a.startswith('__') and not callable(getattr(c, a))]
    for attr in own_attrs:
      if getattr(self, attr) is None and attr in other_attrs:
        setattr(self, attr, getattr(c, attr))
